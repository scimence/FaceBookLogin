
package com.ltsdk_entryDemo;

import java.util.Arrays;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.ltgame.xiyou.friday.R;


public class LogoActivity extends Activity
{
	CallbackManager callbackManager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// facebook初始化
		FacebookSdk.sdkInitialize(this.getApplicationContext());
		AppEventsLogger.activateApp(this);
		
		// facebook登录回调处理逻辑
		callbackManager = CallbackManager.Factory.create();
		LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>()
		{
			@Override
			public void onSuccess(LoginResult loginResult)
			{
				// App code
			}
			
			@Override
			public void onCancel()
			{
				// App code
			}
			
			@Override
			public void onError(FacebookException exception)
			{
				// App code
			}
		});
	}
	
	// 登录响应处理逻辑
	public void Login(View v)
	{
		LoginManager.getInstance().logOut();
		LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends"));
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		// facebook 将登录结果转发到在 onCreate() 中创建的 callbackManager
		callbackManager.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
}
